import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  pageTitle: string = "Choose Menu";

  menuCtrl: FormControl = new FormControl(null, Validators.required);
  selectdateCtrl: FormControl = new FormControl(null, Validators.required);
  selecttiffinitemCtrl: FormControl = new FormControl(null, [
    Validators.required,
    Validators.maxLength(10),
  ]);
  itemSelected: FormControl = new FormControl(null, Validators.required);
 

 menuGroup: FormGroup = new FormGroup({
  menu:this.menuCtrl,
  tiffin: this.selecttiffinitemCtrl,
  selectdate: this.selectdateCtrl
  });
  
  

  constructor() { }

  ngOnInit(): void {
  }

}