import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-customer-entry',
  templateUrl: './customer-entry.component.html',
  styleUrls: ['./customer-entry.component.scss']
  
})
export class CustomerEntryComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  pageTitle: string = "Customer Entry";



  nameCtrl: FormControl = new FormControl(null, Validators.required);
  emailCtrl: FormControl = new FormControl(null, Validators.required);
  passwordCtrl: FormControl = new FormControl(null, [
        Validators.required,
        Validators.maxLength(6),
        Validators.pattern('[0-5]+')
        ]);


  itemSelected: FormControl = new FormControl(null, Validators.required);
 

 entryGroup: FormGroup = new FormGroup({
 name:this.nameCtrl,
 email:this.emailCtrl,
 passwordCtrl:this.passwordCtrl
});
 
  constructor() {
   }

   onSubmit(event: Event) {
     if (this.entryGroup.valid){}
   }

}

